-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2022 at 08:27 AM
-- Server version: 10.5.16-MariaDB
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id20014178_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `jenis_product` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `image` varchar(280) NOT NULL,
  `price` varchar(100) NOT NULL,
  `deskripsi` varchar(280) NOT NULL,
  `status` enum('Pending','Ditolak','Disetujui','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `jenis_product`, `name`, `image`, `price`, `deskripsi`, `status`) VALUES
(17, 'Laptop', 'LENOVO THINKBOOK 14 GEN 4', 'laptop.jpg', 'Rp.25.000.000', 'Laptop gaming\r\nLibas rata kiri kanan\r\nDaya baterai tahan lama', 'Pending'),
(24, 'Baju', 'Crewneck', 'baju.jpg', 'Rp.150.000', 'Terbuat dari bahan tebal\r\ntahan lama dan awet', ''),
(25, 'Furniture', 'Lemari lunar', 'lemari.jpg', 'Rp.1.500.000', 'Kuat dan tahan lama\r\nterbuat dari kayu berkualitas', ''),
(26, 'Furniture', 'Meja', 'meja.jpg', 'Rp.370.000', 'Kuat dan tahan lama terbuat dari kayu berkualitas', ''),
(27, 'Alat tulis', 'Pensil 2B', 'pensil.jpg', 'Rp.2.000', 'Harga murah dan tahan lama', 'Disetujui'),
(36, 'Furniture', 'Meja', 'meja1.jpg', 'Rp.2.000.000', 'Terbuat dari kayu jati\r\nproduk tahan lama\r\n', ''),
(37, 'Jaket', 'Erigo', 'jaket.jpg', 'Rp.200.000', 'Bahan awet dan tidak mudah terkoyak\r\nKualitas bagus', ''),
(38, 'Jaket', 'Jaket Baseball', 'images_(1).jpg', 'Rp.225.000', 'Bahan tebal dan hangat\r\ntidak mudah robek \r\nkualitas bagus', 'Ditolak'),
(40, 'Furniture', 'Meja', 'meja1.jpg', 'Rp.330.000', 'Daya tahan awet tidak mudah rusak\r\nHarga murah', 'Pending'),
(41, 'Laptop', 'Acer', 'laptop.jpg', 'Rp.7.000.000', 'Harga cocok untuk mahasiswa Daya baterai hemat dan tidak cepat habis Ram 8 GB+SSD', 'Disetujui'),
(42, 'Furniture', 'Lemari', 'lemari.jpg', 'Rp.1.450.000', 'Isi dalam luas, tahan lama, tidak mudah rusak, harga terjangkau', ''),
(62, 'Jeans', ' Light Wash', 'jeans.jpg', 'Rp.350.000', 'celana yang lagi ngetren 2022 dan identik dengan classic style.celana slim tidak mudah robek ,harga terjangkau', ''),
(63, 'Handphone', 'Poco x3 pro', 'hp_poco.jpg', 'Rp3.869.000', 'Penyimpanan & RAM\r\n6+128GB/ 8+256GB\r\nLPDDR4X+UFS 3.1\r\n*Penyimpanan dan RAM yang tersedia lebih sedikit dari total memori dikarenakan penyimpanan operasi sistem dan pra-instal software di perangkat.\r\n*Konfigurasi yang tersedia bervariasi antar daerah.\r\n\r\nDimensi\r\nTinggi: 165,3 mm\r', ''),
(64, 'Handphone', 'Realme 8 Pro', 'hp_realme.jpg', 'Rp5.599.000', 'Chipset Qualcomm SM7125 Snapdragon 720G (8 nm) CPU Octa-core (2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver) GPU Adreno 618 MEMORY Card slot microSDXC (dedicated slot) Internal 128GB 8GB RAM UFS 2.1 MAIN CAMERA Quad 108 MP, f/1.9, 26mm (wide), 1/1.52\", 0.7µm, PDAF 8 ', 'Disetujui'),
(65, 'Sepatu', 'Adidas', 'sepatu.jpg', 'Rp.470.000', 'ADIDAS DRAGON WHITE FRANCE ORIGINAL 100 % BNWB MADE IN INDONESIA Kelengakapan (BOX) + (WRAPING PAPER) + (SEPATU ORIGINAL) Brand New With Replaced Box {BNWB}', ''),
(69, 'Sepatu', 'Air Jordan 1 (Black and Gold)', 'jordan.jpg', 'Rp.15.500.000', 'Sepatu pertama rilis yaitu Air Jordan 1 (Black and Gold) yang dikeluarkan tahun 1985', 'Disetujui');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(130) NOT NULL,
  `email` varchar(130) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(130) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(5, 'Irpan', 'irpanbaeg@gmail.com', 'default4.svg', '$2y$10$pRcwaf4rb0XxIuLrJqpgDO/cFq1ERNVp/KftTUWXfUkPvqjBa6CKC', 1, 1, 1670083439),
(6, 'Yuni', 'anggiawahyuni@gmail.com', 'undraw_profile_3.svg', '$2y$10$LTIt3bgK1HDMyj1chJhwqOYnajwQNOfxB5cq4JzmiCr4JX7uqckEi', 2, 1, 1670087295);

-- --------------------------------------------------------

--
-- Table structure for table `user_acces_menu`
--

CREATE TABLE `user_acces_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_acces_menu`
--

INSERT INTO `user_acces_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(130) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `tittle` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `tittle`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'index.php/admin', 'fa-solid fa-gauge-high', 1),
(2, 2, 'Home', 'index.php/home', 'fa-solid fa-house', 1),
(3, 2, 'My Profile', 'index.php/user/profile', 'fa-solid fa-user', 1),
(4, 3, 'Menu Mangement', 'index.php/menu/index', 'fa-solid fa-folder', 1),
(5, 3, 'Submenu Management', 'index.php/menu/submenu', 'fa-solid fa-folder-open', 1),
(7, 1, 'Role', 'index.php/admin/role', 'fa-sharp fa-solid fa-user', 1),
(8, 2, 'Edit Profile', 'index.php/user/edit', 'fa-solid fa-user-pen', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_acces_menu`
--
ALTER TABLE `user_acces_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_acces_menu`
--
ALTER TABLE `user_acces_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
